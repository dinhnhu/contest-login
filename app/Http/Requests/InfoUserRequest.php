<?php

namespace App\Http\Requests;


use Illuminate\Support\Facades\Validator;

class InfoUserRequest
{

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validateRegister($request)
    {
        return Validator::make(
            $request,
            [
                'name'     => 'required|min:3',
                'phone'    => 'required|unique:users',
                'address'  => 'required',
                'password' => 'required|min:6',
                'email'    => 'required|email|unique:users',
            ]);
    }

    public static function validateLogin($request)
    {
        return Validator::make(
            $request,
            [
                'password' => 'required',
                'email'    => 'required|email'
            ]);
    }

    public static function validateMail($request)
    {
        return Validator::make(
            $request,
            [
                'email'    => 'required|email',
            ]);
    }

    public static function validateUpdateInfo($request)
    {
        return Validator::make(
            $request,
            [
                'name'     => 'required|min:3',
                'phone'    => 'required',
                'address'  => 'required',
                'password' => 'required|min:6',
                'email'    => 'required|email',
            ]);
    }
}
