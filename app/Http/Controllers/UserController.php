<?php
/**
 * Created by PhpStorm.
 * User: nhund
 * Date: 21/05/2018
 * Time: 09:33
 */

namespace App\Http\Controllers;


use App\Http\Requests\InfoUserRequest;
use App\Model\User;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function CreateUser(Request $request)
    {
        $validated = InfoUserRequest::validateRegister($request->all());

        if ($validated->fails()) {
            return response()->json(['response_code' => 201, 'data' => $validated->messages(), 'message' => 'the data is invalid']);
        }

        $return = User::addUser($request->all());

        return response()->json(['response_code' => 200, 'data' => $return, 'message' => 'adding successful']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(Request $request)
    {
        $validated = InfoUserRequest::validateUpdateInfo($request->all());

        if ($validated->fails()) {
            return response()->json(['response_code' => 201, 'data' => $validated->messages(), 'message' => 'the data is invalid']);
        }

        $return = User::updateInfoUser($request->all());

        return response()->json(['response_code' => 200, 'data' => $return, 'message' => 'updating successful']);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $validated = InfoUserRequest::validateLogin($request->all());

        if ($validated->fails()) {
            return response()->json(['response_code' => 201, 'data' => $validated->messages(), 'message' => 'the data is invalid or not exist']);
        }

        $login = User::Login($request['email'], $request['password']);

        if ($login) {
            return response()->json(['response_code' => 200, 'data' => $login, 'message' => 'login successful']);
        }

        return response()->json(['response_code' => 201, 'data' => $login, 'message' => 'login failed']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userInfo(Request $request)
    {

        $validated = InfoUserRequest::validateMail($request->all());

        if ($validated->fails()) {
            return response()->json(['response_code' => 201, 'data' => $validated->messages(), 'message' => 'the mail is invalid or not exist']);
        }

        $user = User::userInfo($request['email']);

        if ($user) {
            return response()->json(['response_code' => 200, 'data' => $user, 'message' => 'get info successful']);
        }

        return response()->json(['response_code' => 201, 'data' => $user, 'message' => 'get info  failed']);
    }
}