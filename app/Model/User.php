<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

/**
 * Class User
 * @package App\Model
 */
class User extends Model
{
    /**
     * @param $params
     * @return mixed
     */
    public static function addUser($params)
    {
        $params['created_at'] = Carbon::now()->toDateTimeString();
        $params['password'] = Hash::make($params['password']);

        return \DB::table('users')->insertGetId($params);
    }

    /**
     * @param $params
     * @return mixed
     */
    public static function updateInfoUser($params)
    {

        $params['updated_at'] = Carbon::now()->toDateTimeString();
        $params['password'] = Hash::make($params['password']);

        $update = \DB::table('users')
            ->where('email', $params['email'])
            ->update($params);;

        if ($update) {
            return self::userInfo($params['email']);
        }
        return false;
    }


    /**
     * @param $email
     * @param $password
     * @return bool
     */
    public static function Login($email, $password)
    {
        $user = \DB::table('users')
            ->where('email', $email)
            ->first();

        if ($user) {
            $checkPass = Hash::check($password, $user->password);
            if ($checkPass) {
                return $user;
            }
            return false;
        }

        return false;
    }

    /**
     * @param $email
     * @return mixed
     */
    public static function userInfo($email)
    {
        return \DB::table('users')
            ->where('email', $email)
            ->first();
    }
}
