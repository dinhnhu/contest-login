<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([], function () {
    Route::post('/add', ['uses' => 'UserController@CreateUser']);
    Route::post('/login', ['uses' => 'UserController@login']);
    Route::post('/update', ['uses' => 'UserController@updateUser']);
    Route::post('/info-user', ['uses' => 'UserController@userInfo']);
});